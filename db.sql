/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.12-log : Database - elos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`elos` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `elos`;

/*Table structure for table `elos_analisis` */

DROP TABLE IF EXISTS `elos_analisis`;

CREATE TABLE `elos_analisis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(11) NOT NULL,
  `status_analisis` enum('disetujui','tidak_disetujui') DEFAULT 'disetujui',
  `komentar` text NOT NULL,
  `tgl` date DEFAULT NULL,
  `id_analis` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_elos_analisis` (`id_pengajuan`),
  KEY `pk_elos_id_analis` (`id_analis`),
  CONSTRAINT `pk_elos_id_analis` FOREIGN KEY (`id_analis`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_elos_analisis` FOREIGN KEY (`id_pengajuan`) REFERENCES `elos_nasabah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `elos_analisis` */

/*Table structure for table `elos_analisis_konsumen` */

DROP TABLE IF EXISTS `elos_analisis_konsumen`;

CREATE TABLE `elos_analisis_konsumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(11) NOT NULL,
  `status_analisis` enum('disetujui','tidak_disetujui') DEFAULT 'disetujui',
  `komentar` text NOT NULL,
  `tgl` date DEFAULT NULL,
  `id_analis` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_elos_analisis_konsumen` (`id_pengajuan`),
  KEY `pk_elos_id_analis_konsumen` (`id_analis`),
  CONSTRAINT `pk_elos_id_analis_konsumen` FOREIGN KEY (`id_analis`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_elos_analisis_konsumen` FOREIGN KEY (`id_pengajuan`) REFERENCES `elos_nasabah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `elos_analisis_konsumen` */

/*Table structure for table `elos_ao` */

DROP TABLE IF EXISTS `elos_ao`;

CREATE TABLE `elos_ao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(11) NOT NULL,
  `status_analisis` enum('disetujui','tidak_disetujui') DEFAULT 'disetujui',
  `komentar` text NOT NULL,
  `tgl` date NOT NULL,
  `id_ao` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_elos_ao` (`id_pengajuan`),
  KEY `pk_elos_id_ao` (`id_ao`),
  CONSTRAINT `pk_elos_id_ao` FOREIGN KEY (`id_ao`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_elos_ao` FOREIGN KEY (`id_pengajuan`) REFERENCES `elos_nasabah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `elos_ao` */

/*Table structure for table `elos_ao_konsumen` */

DROP TABLE IF EXISTS `elos_ao_konsumen`;

CREATE TABLE `elos_ao_konsumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(11) NOT NULL,
  `status_analisis` enum('disetujui','tidak_disetujui') DEFAULT 'disetujui',
  `komentar` text NOT NULL,
  `tgl` date DEFAULT NULL,
  `id_ao` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_elos_ao_konsumen` (`id_pengajuan`),
  KEY `pk_elos_id_ao_konsumen` (`id_ao`),
  CONSTRAINT `pk_elos_id_ao_konsumen` FOREIGN KEY (`id_ao`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_elos_ao_konsumen` FOREIGN KEY (`id_pengajuan`) REFERENCES `elos_nasabah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `elos_ao_konsumen` */

/*Table structure for table `elos_bm` */

DROP TABLE IF EXISTS `elos_bm`;

CREATE TABLE `elos_bm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(11) NOT NULL,
  `status_analisis` enum('disetujui','tidak_disetujui') DEFAULT 'disetujui',
  `komentar` text NOT NULL,
  `tgl` date NOT NULL,
  `id_bm` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_elos_bm` (`id_pengajuan`),
  KEY `pk_elos_id_bm` (`id_bm`),
  CONSTRAINT `pk_elos_id_bm` FOREIGN KEY (`id_bm`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_elos_bm` FOREIGN KEY (`id_pengajuan`) REFERENCES `elos_nasabah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `elos_bm` */

/*Table structure for table `elos_bm_konsumen` */

DROP TABLE IF EXISTS `elos_bm_konsumen`;

CREATE TABLE `elos_bm_konsumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(11) NOT NULL,
  `status_analisis` enum('disetujui','tidak_disetujui') DEFAULT 'disetujui',
  `komentar` text NOT NULL,
  `tgl` date NOT NULL,
  `id_bm` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_elos_bm_konsumen` (`id_pengajuan`),
  KEY `pk_elos_id_bm_konsumen` (`id_bm`),
  CONSTRAINT `pk_elos_id_bm_konsumen` FOREIGN KEY (`id_bm`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_elos_bm_konsumen` FOREIGN KEY (`id_pengajuan`) REFERENCES `elos_nasabah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `elos_bm_konsumen` */

/*Table structure for table `elos_nasabah` */

DROP TABLE IF EXISTS `elos_nasabah`;

CREATE TABLE `elos_nasabah` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama_debitur` varchar(255) NOT NULL,
  `key_person` varchar(255) NOT NULL,
  `alamat_usaha` varchar(255) NOT NULL,
  `alamat_rumah` varchar(255) NOT NULL,
  `nomor_ktp` varchar(255) NOT NULL,
  `masa_berlaku_ktp` date NOT NULL,
  `bidang_usaha` varchar(255) NOT NULL,
  `sektor_ekonomi` varchar(255) NOT NULL,
  `npwp` varchar(255) NOT NULL,
  `akta_pendirian` varchar(255) NOT NULL,
  `situ` varchar(255) NOT NULL,
  `siup` varchar(255) NOT NULL,
  `siuug` varchar(255) NOT NULL,
  `tdp` varchar(255) NOT NULL,
  `maksimum_kredit` bigint(255) NOT NULL,
  `keperluan_kredit` varchar(255) NOT NULL,
  `jangka_waktu_kredit` varchar(255) NOT NULL,
  `nomor_surat_permohonan_kredit` varchar(255) NOT NULL,
  `tanggal_surat_permohonan_kredit` date NOT NULL,
  `produk_yang_dijual` varchar(255) NOT NULL,
  `rata_rata_penjualan_perbulan` bigint(255) NOT NULL,
  `rata_pembelian_bahan_baku` bigint(255) NOT NULL,
  `laba_kotor_perbulan` bigint(255) NOT NULL,
  `biaya_operasional_per_bulan` bigint(255) NOT NULL,
  `biaya_hidup_per_bulan` bigint(255) NOT NULL,
  `biaya_lain_lain` bigint(255) NOT NULL,
  `total_biaya` bigint(255) NOT NULL,
  `laba_bersih_per_bulan` bigint(255) NOT NULL,
  `rata_rata_laba_bersih` decimal(10,0) NOT NULL,
  `lama_usaha` int(11) NOT NULL,
  `legalitas_usaha` varchar(255) NOT NULL,
  `legalitas_agunan` varchar(255) NOT NULL,
  `status_kemampuan_manajemen` enum('Baik','Tidak') DEFAULT 'Baik',
  `kemampuan_manajemen` varchar(255) NOT NULL,
  `status_kondisi_prospek_usaha` enum('Baik','Tidak') DEFAULT 'Baik',
  `kondisi_prospek_usaha` varchar(255) NOT NULL,
  `status_kondisi_keuangan_usaha` enum('Baik','Tidak') DEFAULT 'Baik',
  `kondisi_keuangan_usaha` varchar(255) NOT NULL,
  `status_kondisi_pemasaran_usaha` enum('Baik','Tidak') DEFAULT 'Baik',
  `kondisi_pemasaran_usaha` varchar(255) NOT NULL,
  `status_kondisi_produksi_usaha` enum('Baik','Tidak') DEFAULT 'Baik',
  `kondisi_produksi_usaha` varchar(255) NOT NULL,
  `status_resiko_pemasaran_persaingan` enum('Ada','Tidak') DEFAULT 'Ada',
  `resiko_pemasaran_persaingan` varchar(255) NOT NULL,
  `status_resiko_produksi_bahan_baku` enum('Ada','Tidak') DEFAULT 'Ada',
  `resiko_produksi_bahan_baku` varchar(255) NOT NULL,
  `status_resiko_keuangan_usaha` enum('Ada','Tidak') DEFAULT 'Ada',
  `resiko_keuangan_usaha` varchar(255) NOT NULL,
  `status_resiko_manajemen` enum('Ada','Tidak') DEFAULT 'Ada',
  `resiko_manajemen` varchar(255) NOT NULL,
  `jenis_agunan` varchar(255) NOT NULL,
  `luas_tanah` int(11) NOT NULL,
  `luas_bangunan` int(11) NOT NULL,
  `shm` varchar(255) NOT NULL,
  `imb` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `harga_pasar_tanah` int(11) NOT NULL,
  `harga_pasar_bangunan` int(11) NOT NULL,
  `status` enum('Diajukan','Dianalisis','Ditaksir','Disetujui','Diajukan ke spv','Diajukan ke bm') DEFAULT 'Diajukan',
  `image` varchar(255) NOT NULL,
  `id_ao` int(11) NOT NULL,
  `tanggal_pengajuan` datetime NOT NULL,
  PRIMARY KEY (`id`,`kondisi_keuangan_usaha`),
  KEY `pk_elos_id_ao_nasabah` (`id_ao`),
  CONSTRAINT `pk_elos_id_ao_nasabah` FOREIGN KEY (`id_ao`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `elos_nasabah` */

/*Table structure for table `elos_nasabah_konsumen` */

DROP TABLE IF EXISTS `elos_nasabah_konsumen`;

CREATE TABLE `elos_nasabah_konsumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_debitur` varchar(255) DEFAULT NULL,
  `alamat_rumah` varchar(255) NOT NULL,
  `nomor_ktp` varchar(255) NOT NULL,
  `masa_berlaku_ktp` date NOT NULL,
  `npwp` varchar(50) NOT NULL,
  `pekerjaan` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `nama_perusahaan` varchar(255) NOT NULL,
  `alamat_perusahaan` varchar(255) NOT NULL,
  `no_telp_perusahaan` varchar(255) NOT NULL,
  `maksimum_kredit` bigint(255) NOT NULL,
  `keperluan_kredit` varchar(255) NOT NULL,
  `jangka_waktu_kredit` varchar(255) NOT NULL,
  `nomor_surat_permohonan_kredit` varchar(255) NOT NULL,
  `tanggal_surat_permohonan_kredit` date NOT NULL,
  `pendapatan_perbulan` bigint(255) NOT NULL,
  `pendapatan_lain` bigint(255) NOT NULL,
  `laba_kotor_perbulan` bigint(255) NOT NULL,
  `biaya_hidup_per_bulan` bigint(255) NOT NULL,
  `biaya_lain_lain` bigint(255) NOT NULL,
  `total_pendapatan` bigint(255) NOT NULL,
  `pendapatan_bersih_per_bulan` bigint(255) NOT NULL,
  `rata_rata_pendapatan_bersih` bigint(255) NOT NULL,
  `lama_usaha` int(11) NOT NULL,
  `legalitas_usaha` varchar(255) NOT NULL,
  `legalitas_agunan` varchar(255) NOT NULL,
  `status_kemampuan_manajemen` enum('Baik','Tidak') DEFAULT 'Baik',
  `kemampuan_manajemen` varchar(255) NOT NULL,
  `status_kondisi_keuangan` enum('Baik','Tidak') DEFAULT 'Baik',
  `kondisi_keuangan` varchar(255) NOT NULL,
  `status_konsistensi_sikap` enum('Baik','Tidak') DEFAULT 'Baik',
  `konsistensi_sikap` varchar(255) NOT NULL,
  `status_hutang_dimiliki` enum('Ada','Tidak') DEFAULT 'Tidak',
  `hutang_dimiliki` varchar(255) NOT NULL,
  `status_kewajiban_listrik_air` enum('Baik','Tidak') DEFAULT 'Baik',
  `kewajiban_listrik_air` varchar(255) NOT NULL,
  `status_perilaku_bermasyarakat` enum('Baik','Tidak') DEFAULT 'Baik',
  `perilaku_bermasyarakat` varchar(255) NOT NULL,
  `status_resiko_keuangan` enum('Ada','Tidak') DEFAULT 'Tidak',
  `resiko_keuangan` varchar(255) NOT NULL,
  `status_resiko_key_person` enum('Ada','Tidak') DEFAULT 'Tidak',
  `resiko_key_person` varchar(255) NOT NULL,
  `status_keterangan_perbuatan_kriminal` enum('Ada','Tidak') DEFAULT 'Tidak',
  `keterangan_perbuatan_kriminal` varchar(255) NOT NULL,
  `jenis_agunan` varchar(255) NOT NULL,
  `luas_tanah` int(11) NOT NULL,
  `luas_bangunan` int(11) NOT NULL,
  `shm` varchar(255) NOT NULL,
  `imb` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `harga_pasar_tanah` int(11) NOT NULL,
  `harga_pasar_bangunan` int(11) NOT NULL,
  `status` enum('Diajukan','Dianalisis','Ditaksir','Disetujui','Diajukan ke spv','Diajukan ke bm') DEFAULT 'Diajukan',
  `image` varchar(255) DEFAULT NULL,
  `id_ao` int(11) NOT NULL,
  `tanggal_pengajuan` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_elos_id_ao_nasabah_konsumen` (`id_ao`),
  CONSTRAINT `pk_elos_id_ao_nasabah_konsumen` FOREIGN KEY (`id_ao`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `elos_nasabah_konsumen` */

/*Table structure for table `elos_spv` */

DROP TABLE IF EXISTS `elos_spv`;

CREATE TABLE `elos_spv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(11) NOT NULL,
  `status_analisis` enum('disetujui','tidak_disetujui') DEFAULT 'disetujui',
  `komentar` text NOT NULL,
  `tgl` date NOT NULL,
  `id_spv` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_elos_spv` (`id_pengajuan`),
  KEY `pk_elos_id_spv` (`id_spv`),
  CONSTRAINT `pk_elos_id_spv` FOREIGN KEY (`id_spv`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_elos_spv` FOREIGN KEY (`id_pengajuan`) REFERENCES `elos_nasabah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `elos_spv` */

/*Table structure for table `elos_spv_konsumen` */

DROP TABLE IF EXISTS `elos_spv_konsumen`;

CREATE TABLE `elos_spv_konsumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(11) NOT NULL,
  `status_analisis` enum('disetujui','tidak_disetujui') DEFAULT 'disetujui',
  `komentar` text NOT NULL,
  `tgl` date DEFAULT NULL,
  `id_spv` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_elos_spv_konsumen` (`id_pengajuan`),
  KEY `pk_elos_id_spv_konsumen` (`id_spv`),
  CONSTRAINT `pk_elos_id_spv_konsumen` FOREIGN KEY (`id_spv`) REFERENCES `elos_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pk_elos_spv_konsumen` FOREIGN KEY (`id_pengajuan`) REFERENCES `elos_nasabah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `elos_spv_konsumen` */

/*Table structure for table `elos_users` */

DROP TABLE IF EXISTS `elos_users`;

CREATE TABLE `elos_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `tipe` enum('AO','SPV','APPRAISAL','BM','ADMIN','ANALIS') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `elos_users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
